output "elastic_ip_id" {
  description = "ID of the Elastic IP"
  value       = aws_eip.one.id
}

output "elastic_public_ip" {
  description = "Public IP address of the Elastic IP"
  value       = aws_eip.one.public_ip
}
